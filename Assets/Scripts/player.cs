﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    public float velocidadY;
    public float maxY;
    private float posY;
    private float direction;
    private Vector3 newPosition = new Vector3(0,0,0); 
    
    
    void Update()
    {
        direction = Input.GetAxis("Vertical");

        Debug.Log(transform.position.y);
        Debug.Log(transform.rotation.y);
 
        posY = transform.position.y + direction*velocidadY*Time.deltaTime;
 
        newPosition.x = transform.position.x;
        newPosition.y = posY;
        newPosition.z = transform.position.z;
        transform.position = newPosition;

        if(posY>maxY){
            posY = maxY;
        }else if(posY<-maxY){
            posY = -maxY;
        }
 
        transform.position = new Vector3(transform.position.x, posY, transform.position.z);     
    }
}
